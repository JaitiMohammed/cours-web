import React from 'react';
import ReactDOM from 'react-dom';
import  CommentDetail  from './CommentDetail';
import faker from 'faker';
import ApprovalCard from './ApprovalCard';

const App =() => {
    return (
        <div className="ui container comments">
            <ApprovalCard>
            <CommentDetail 
                author="Sam"  
                date="Today at 4:00PM" 
                text="Nice picture"
                avatar={faker.image.avatar()}
            /></ApprovalCard>
            
            <ApprovalCard>
            <CommentDetail 
                author="John" 
                date="Today at 5:10PM" 
                text="Good luck"
                avatar={faker.image.avatar()}
            /></ApprovalCard>
            
            <ApprovalCard>
            <CommentDetail 
                author="San" 
                date="Today at 4:50PM" 
                text="Nice bro"
                avatar={faker.image.avatar()}
            /></ApprovalCard>
            
            <ApprovalCard><CommentDetail 
                author="loka" 
                date="Lundi 2 oct at 8:00AM" 
                text="Go to hell"
                avatar={faker.image.avatar()}
            /></ApprovalCard>
           
            
            <ApprovalCard>
            <CommentDetail 
                author="sofi" 
                date="Now at 2:00PM" 
                text="Nice my bro"
                avatar={faker.image.avatar()}
            />
            </ApprovalCard>
        </div>

    );
}

ReactDOM.render(<App />, document.getElementById('root'));

